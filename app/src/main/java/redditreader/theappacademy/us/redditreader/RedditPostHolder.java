package redditreader.theappacademy.us.redditreader;

import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class RedditPostHolder extends RecyclerView.ViewHolder {
    public TextView titleText;


    public RedditPostHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
