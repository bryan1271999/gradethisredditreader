package redditreader.theappacademy.us.redditreader;


import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.FloatMath;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.List;

public class RedditWebActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private List<RedditPost> redditPosts;
    public SensorManager sensorManager;
    public Sensor accelerometer;
    public float acceleration;
    public float currentAcceleration;
    public float previousAcceleration;
    public static int randomNumber = 0;
    public static int i = 0;
    public static int p = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        i = 0;
        p = 0;
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        previousAcceleration = SensorManager.GRAVITY_EARTH;
        setContentView(R.layout.activity_reddit_web);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        redditPosts = RedditPostParser.getInstance().redditPosts;

        Intent intent = getIntent();
        Uri redditUri = intent.getData();

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                RedditPost redditPost;
                redditPost = redditPosts.get(position);
                return RedditWebFragment.newFragment(redditPost.url);
            }

            @Override
            public int getCount() {
                return redditPosts.size();
            }
        });

        for (int index = 0; index < redditPosts.size(); index++) {
            if (redditPosts.get(index).url.equals(redditUri.toString())) {
                viewPager.setCurrentItem(index);
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            previousAcceleration = currentAcceleration;
            currentAcceleration = FloatMath.sqrt(x * x + y * y + z * z);
            float delta = currentAcceleration - previousAcceleration;
            acceleration = acceleration * 0.9f + delta;
            Intent intent = getIntent();
            Uri redditUri = intent.getData();

            FragmentManager fragmentManager = getSupportFragmentManager();
            if(acceleration >= 10 && acceleration <= 15) {
                viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
                    @Override
                    public Fragment getItem(int position) {
                        RedditPost redditPost;
                        p = 0;
                        p++;
                        redditPost = redditPosts.get(position + p);
                        return RedditWebFragment.newFragment(redditPost.url);
                    }

                    @Override
                    public int getCount() {
                        return redditPosts.size();
                    }
                });

                for (int index = 0; index < redditPosts.size(); index++) {
                    if (redditPosts.get(index).url.equals(redditUri.toString())) {
                        viewPager.setCurrentItem(index);
                        break;
                    }
                }
            }
            else if(acceleration >= 25) {
                viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
                    @Override
                    public Fragment getItem(int position) {
                        RedditPost redditPost;
                        randomNumber = 0;
                        randomNumber = 0 + (int)(Math.random() * 10);
                        position = randomNumber;
                        redditPost = redditPosts.get(randomNumber);
                        return RedditWebFragment.newFragment(redditPost.url);
                    }

                    @Override
                    public int getCount() {
                        return redditPosts.size();
                    }
                });

                for (int index = 0; index < redditPosts.size(); index++) {
                    if (redditPosts.get(index).url.equals(redditUri.toString())) {
                        viewPager.setCurrentItem(index);
                        break;
                    }
                }
            }
    }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
};
}
